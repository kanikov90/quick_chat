<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        Авторизация
    </title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="row">
    <div class="col-sm"></div>
    <div class="col-sm">
        <form action="/login" method="post">
            <div class="form-group">
                Имя пользователя
                <label for="inputUsername"></label>
                 <@spring.formInput "loginForm.username" "class=\"form-control\" id=\"inputUsername\" placeholder=\"Введите имя\"" "username"/>
            </div>
            <button type="submit" class="btn btn-primary">Войти в чат</button>
        </form>
    </div>
    <div class="col-sm"></div>
</div>

</body>
</html>