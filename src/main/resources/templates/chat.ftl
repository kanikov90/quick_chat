<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        Quick chat
    </title>
    <script type="text/javascript" src="/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/sockjs.min.js"></script>
    <script type="text/javascript" src="/js/stomp.min.js"></script>
    <script>
        var stompClient = null;
        var username = $.cookie("username");
        var userId = $.cookie("userId");

        function connect() {
            var socket = new SockJS('/quickChat');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function () {
                stompClient.subscribe('/topic/quickChat', function (message) {
                     addMessageInChat(JSON.parse(message.body));
                });
                stompClient.subscribe('/topic/connectUser', function (onlineUsers) {
                    updateOnlineUsers(JSON.parse(onlineUsers.body));
                });
                stompClient.subscribe('/topic/disconnectUser', function (onlineUsers) {
                    updateOnlineUsers(JSON.parse(onlineUsers.body));
                });
                stompClient.send("/app/connectUser", {}, JSON.stringify({
                    'id': userId,
                    'username': username
                }));
            });
        };

        window.onload = function() {
            $('.user-container').html('');
            $('.user-container').append('Привет, ' + username + '! | <a href="/logout" onclick="disconnect();"> Выйти</a><br/><br/>');
        };

        connect();

        function disconnect() {
            if (stompClient != null) {
                stompClient.send("/app/disconnectUser", {}, JSON.stringify({
                    'id': userId,
                    'username': username
                    }
                ));
                stompClient.disconnect();
            }
        }

        function updateOnlineUsers(onlineUsers) {
            $('.onlineUsers').html("");

            $.each(onlineUsers, function (i, onlineUser) {
                $('.onlineUsers').append(onlineUser.username + '</br>');
            })
        }

        function addMessageInChat(message) {
            if (typeof message.content !== 'undefined') {
                $("#listMessages").append('<li><b>' +
                        message.username + '</b>' + ' | ' + '<i>' + new Date(message.createDttm).toLocaleDateString() + ' ' + new Date(message.createDttm).toLocaleTimeString()
                        + '</i></br>' + message.content + '</li></br>');
                $("#listMessages").animate({scrollTop: $("#listMessages").prop("scrollHeight")}, 500);
            }
        }

        function sendMessage() {
            var message = $("#messageText").val();
            if (message != '') {
                stompClient.send("/app/quickChat", {}, JSON.stringify({
                    'content': message,
                    'user': {
                        'id': userId,
                        'username': username
                    }
                }));
            }
            $("#messageText").val("");
        }
    </script>
</head>
<body>
    <div class="user-container"></div>

    <h3>Quick chat</h3>

    <ul id="listMessages" style="width: 400px; height: 350px; overflow: auto; border: 1px solid #000000;">
        <#list lastMessages as message>
            <li>
                <b>${message.user.username}</b> | <i>${message.createDttm}</i></br>
                ${message.content}</br></br>
            </li>
        </#list>
    </ul>

    <form id="messageForm" name="messageForm">
        <div class="input-message" >
            <input type="text" class="form-control" placeholder="Enter Message" id="messageText" autofocus=""/>
            <button class="btn btn-info" type="button" onclick="sendMessage();">SEND</button>
        </div>
    </form>

    <br/>Пользователи онлайн:
    <div class="onlineUsers"></div>
</body>
</html>