create table qc_users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) NOT NULL
);

create unique index qc_sers_username_uindex
  on qc_users (username);

create table qc_messages (
  id SERIAL PRIMARY KEY,
  content TEXT NOT NULL,
  create_dttm timestamp NOT NULL,
  user_id INT references qc_users(id)
);
