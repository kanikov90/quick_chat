package com.quickchat.web.controller;

import com.quickchat.web.form.LoginForm;
import com.quickchat.web.model.UserModel;
import com.quickchat.web.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AuthController implements SecureController {

    private final UserRepository userRepository;

    @GetMapping({"/", "/login"})
    public String toLogin(Model model, HttpServletRequest request) {
        UserModel user = getUser(request);
        if (user != null) {
            return "redirect:/chat";
        }
        logout(request);
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }

    @PostMapping("/login")
    public String postLogin(
            @ModelAttribute("loginForm") @Valid LoginForm loginForm,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        String username = loginForm.getUsername();
        if (username != "") {
            UserModel user = userRepository.findByUsername(username).orElse(new UserModel());
            if (user.getId() == null) {
                user.setUsername(username);
                user = userRepository.save(user);
            }
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            Cookie cookieUserId = new Cookie("userId", user.getId().toString());
            Cookie cookieUsername = new Cookie("username", user.getUsername());
            response.addCookie(cookieUserId);
            response.addCookie(cookieUsername);

            return "redirect:/chat";
        }

        logout(request);
        return "redirect:/login";
    }

    @GetMapping("/logout")
    public String logoutUser(HttpServletRequest request, HttpServletResponse response) {
        logout(request);
        eraseCookie(request, response);
        return "redirect:/login";
    }

    private void eraseCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null)
            for (Cookie cookie : cookies) {
                cookie.setValue("");
                cookie.setPath("/");
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
    }
}