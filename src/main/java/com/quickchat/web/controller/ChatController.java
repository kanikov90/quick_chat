package com.quickchat.web.controller;

import com.quickchat.web.model.MessageModel;
import com.quickchat.web.model.UserModel;
import com.quickchat.web.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ChatController implements SecureController {

    private final MessageRepository messageRepository;

    @GetMapping("/chat")
    public String getChat(Model model, HttpServletRequest request) {
        UserModel user = getUser(request);

        if (user == null) {
            return "redirect:/login";
        }
        List<MessageModel> lastMessages = messageRepository.findFirst5ByOrderByCreateDttmDesc();
        lastMessages.sort(Comparator.comparing(MessageModel::getCreateDttm));
        model.addAttribute("lastMessages", lastMessages);
        return "chat";
    }

    @MessageMapping("/quickChat")
    @SendTo("/topic/quickChat")
    public String save(MessageModel messageModel) {

        messageModel.setCreateDttm(new Timestamp(new Date().getTime()));
        MessageModel saveMessage = messageRepository.save(messageModel);
        return saveMessage.toString();
    }

}
