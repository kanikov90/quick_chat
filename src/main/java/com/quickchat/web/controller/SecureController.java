package com.quickchat.web.controller;

import com.quickchat.web.model.UserModel;

import javax.servlet.http.HttpServletRequest;

public interface SecureController{

    default UserModel getUser(HttpServletRequest request){
        return (UserModel) request.getSession().getAttribute("user");
    }

    default void logout(HttpServletRequest request){
        request.getSession().removeAttribute("user");
    }
}
