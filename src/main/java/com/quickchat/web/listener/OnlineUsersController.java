package com.quickchat.web.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quickchat.web.model.UserModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import java.util.List;

@AllArgsConstructor
@Data
@Controller
public class OnlineUsersController {

    private List<UserModel> onlineUsers;
    private ObjectMapper objectMapper;

    @MessageMapping("/connectUser")
    @SendTo("/topic/connectUser")
    public String addConnectedUser(UserModel user) throws JsonProcessingException {
        if (!onlineUsers.contains(user)) {
            onlineUsers.add(user);
        }
        return objectMapper.writeValueAsString(onlineUsers);
    }

    @MessageMapping("/disconnectUser")
    @SendTo("/topic/disconnectUser")
    public String removeDisconnectedUser(UserModel user) throws JsonProcessingException {
        if (onlineUsers.contains(user)) {
            onlineUsers.remove(user);
        }
        return objectMapper.writeValueAsString(onlineUsers);
    }
}
