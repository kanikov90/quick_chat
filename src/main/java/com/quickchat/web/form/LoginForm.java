package com.quickchat.web.form;

import lombok.Data;

@Data
public class LoginForm {

    private String username;
}
