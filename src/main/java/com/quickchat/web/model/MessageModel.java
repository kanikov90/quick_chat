package com.quickchat.web.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "qc_messages")
public class MessageModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String content;

    @Column(name = "create_dttm")
    private Timestamp createDttm;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel user;

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ",\"content\":\"" + content + '\"' +
                ",\"createDttm\":\"" + createDttm + '\"' +
                ",\"userId\":\"" + user.getId() + "\"" +
                ",\"username\":\"" + user.getUsername() + "\"" +
                '}';
    }
}
