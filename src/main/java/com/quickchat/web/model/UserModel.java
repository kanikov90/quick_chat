package com.quickchat.web.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "qc_users")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;
}
