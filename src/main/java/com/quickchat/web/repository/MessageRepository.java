package com.quickchat.web.repository;

import com.quickchat.web.model.MessageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<MessageModel, Long> {

    List<MessageModel> findFirst5ByOrderByCreateDttmDesc();
}