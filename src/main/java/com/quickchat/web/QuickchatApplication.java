package com.quickchat.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuickchatApplication{

    public static void main(String[] args) {
        SpringApplication.run(QuickchatApplication.class, args);
    }

}

